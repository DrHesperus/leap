package com.legacy.leap;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.util.HashMap;

public class LeapEntityEvents
{
	public static HashMap<String, Boolean> uuidHasJumpedMap = new HashMap<String, Boolean>();

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event)
	{
		LivingEntity entity = event.getEntityLiving();

		if (entity instanceof PlayerEntity && uuidHasJumpedMap.containsKey(entity.getUniqueID().toString()))
		{
			float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(LeapRegistry.LEAPING, entity);
			boolean usedMidAirJump = uuidHasJumpedMap.get(entity.getUniqueID().toString());
			boolean playerJumping = ObfuscationReflectionHelper.getPrivateValue(LivingEntity.class, entity, "field_70703_bu");
			boolean canJump = !entity.isOnGround() && !usedMidAirJump;

			// Reset jumps
			if (entity.isOnGround())
			{
				uuidHasJumpedMap.put(entity.getUniqueID().toString(), false);
			}

			if (!(enchantmentLevel > 0))
				return;

			// if the entity can jump, jump
			if (canJump && !entity.isElytraFlying() && !((PlayerEntity) entity).abilities.isFlying)
			{
				if (playerJumping && entity.getMotion().getY() < 0)
				{
					entity.setMotion(entity.getMotion().getX(), 0.6D, entity.getMotion().getZ());
					uuidHasJumpedMap.put(entity.getUniqueID().toString(), true);

					entity.playSound(SoundEvents.ENTITY_ENDER_DRAGON_FLAP, 0.3F, 2.0F);
					for (int i = 0; i < 20; ++i)
					{
						double d0 = entity.world.rand.nextGaussian() * 0.02D;
						double d1 = entity.world.rand.nextGaussian() * 0.02D;
						double d2 = entity.world.rand.nextGaussian() * 0.02D;

						entity.world.addParticle(ParticleTypes.POOF, entity.getPosX() + (double) (entity.world.rand.nextFloat() * entity.getWidth() * 2.0F) - (double) entity.getWidth() - d0 * 10.0D, entity.getPosY() - d1 * 10.0D, entity.getPosZ() + (double) (entity.world.rand.nextFloat() * entity.getWidth() * 2.0F) - (double) entity.getWidth() - d2 * 10.0D, d0, d1, d2);
					}
				}

			}
		}
		else if (entity instanceof PlayerEntity)
		{
			uuidHasJumpedMap.put(entity.getUniqueID().toString(), false);
		}
	}

	@SubscribeEvent
	public void onLivingFall(LivingFallEvent event)
	{
		float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(LeapRegistry.LEAPING, event.getEntityLiving());

		if (enchantmentLevel > 0 && uuidHasJumpedMap.containsKey(event.getEntityLiving().getUniqueID().toString()) && uuidHasJumpedMap.get(event.getEntityLiving().getUniqueID().toString()))
		{
			int i = MathHelper.ceil((event.getDistance() - 3.0F - enchantmentLevel) * event.getDamageMultiplier());
			event.setDistance(i);
		}
	}
}
